import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyCompComponent } from './my-comp.component';



@NgModule({
  declarations: [
    MyCompComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    MyCompComponent
  ]
})
export class MyCompModule { }
